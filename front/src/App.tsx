import React from 'react';
import Edit from './components/edit/Edit';
import Register from './components/register/Register';
import List from './components/list/List';
// @ts-ignore
import { CssBaseline, ThemeProvider } from '@mui/material'

import { lightTheme } from './themes'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className="App">
        <ThemeProvider theme={lightTheme}>
          <CssBaseline />
          <Router>
            <Switch>
              <Route exact path="/edit" component={Edit} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/list" component={List} />
            </Switch>
          </Router>
        </ThemeProvider>
    </div>
  );
}

export default App;
