import React from 'react'
import ClienteAxios from '../../axios/ClienteAxios'
import { Box, Button, Grid, MenuItem, TextField, Typography } from '@mui/material'
import { useForm, SubmitHandler } from "react-hook-form";

interface FormData {
    firstName: string,
    secondName: string,
    lastNameOne: string,
    lastNameTwo: string,
    email: string,
    countryJob: string,
    identityType: string,
    identityNumber: string,
    admissionDate: string,
    area: string,
    state: string,
    registerDate: string,
}

const Register = () => {

    const { register, handleSubmit, watch, formState: { errors } } = useForm<FormData>();
    console.log('error', errors);

    const onRegisterEmployee = (data: FormData) => {
        console.log({ data });
        data.email = 'email'
        try {
            const response = ClienteAxios.post('employee', {data})
        } catch (error) {
            console.log('Error', error);
        }

    }

    return (
        <div style={{ width: '100%', marginTop: '50px' }}>
            <form onSubmit={handleSubmit(onRegisterEmployee)} noValidate>
                <Box sx={{ width: '80%', padding: '10px 20px', marginLeft: 'auto', marginRight: 'auto' }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant='h1' component="h1">Crear Empleado</Typography>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField label="Primer nombre" type="text" variant="filled" fullWidth
                                {...register('firstName', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.firstName}
                                helperText={errors.firstName?.message} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Segundo nombre" type="text" variant="filled" fullWidth
                                {...register('secondName', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.secondName}
                                helperText={errors.secondName?.message} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Primer apellido" type="text" variant="filled" fullWidth
                                {...register('lastNameOne', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.lastNameOne}
                                helperText={errors.lastNameOne?.message}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Segundo apellido" type="text" variant="filled" fullWidth
                                {...register('lastNameTwo', {
                                    maxLength: { value: 50, message: 'Maximo 50 caracteres' }
                                })}
                                error={!!errors.lastNameTwo}
                                helperText={errors.lastNameTwo?.message}
                            />
                        </Grid>
                        <Grid item xs={6} >
                            <div style={{ backgroundColor: '#eaeaea', padding: '17px', display: 'flex', justifyContent: 'space-between' }}>
                                <label style={{ color: '#727272' }}>País de empleo:</label>
                                <select
                                    {...register('countryJob')}
                                    name="countryJob"
                                >
                                    <option value="Colombia">Colombia</option>
                                    <option value="USA">USA</option>
                                </select>
                            </div>
                        </Grid>
                        <Grid item xs={6} >
                            <div style={{ backgroundColor: '#eaeaea', padding: '17px', display: 'flex', justifyContent: 'space-between' }}>
                                <label style={{ color: '#727272' }}>Tipo de identificación:</label>
                                <select
                                    {...register('identityType')}
                                    name="identityType"
                                >
                                    <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>
                                    <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                                    <option value="Pasaporte">Pasaporte</option>
                                    <option value="Permiso Especial">Permiso Especial</option>
                                </select>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Número de identificación" type="text" variant="filled" fullWidth
                                {...register('identityNumber', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.identityNumber}
                                helperText={errors.identityNumber?.message}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ backgroundColor: '#eaeaea', padding: '17px', display: 'flex', justifyContent: 'space-between' }} >
                                <label style={{ color: '#727272' }}>Fecha de admisión</label>
                                <input type="date"
                                    {...register('admissionDate', {
                                        required: 'Este campo es requerido',
                                        maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                    })}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Área" type="text" variant="filled" fullWidth
                                {...register('area', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.area}
                                helperText={errors.area?.message}
                            />
                        </Grid>
                        <Grid item xs={6} >
                            <div style={{ backgroundColor: '#eaeaea', padding: '17px', display: 'flex', justifyContent: 'space-between' }}>
                                <label style={{ color: '#727272' }}>Área:</label>
                                <select
                                    {...register('area')}
                                    name="area"
                                >
                                    <option value="Administración">Administración</option>
                                    <option value="Financiera">Financiera</option>
                                    <option value="Compras">Compras</option>
                                    <option value="Infraestructura">Infraestructura</option>
                                    <option value="Operación">Operación</option>
                                    <option value="Talento Humano">Talento Humano</option>
                                    <option value="Servicios Varios">Servicios Varios</option>

                                </select>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Estado" type="text" variant="filled" fullWidth
                                {...register('state', {
                                    required: 'Este campo es requerido',
                                    maxLength: { value: 20, message: 'Maximo 20 caracteres' }
                                })}
                                error={!!errors.state}
                                helperText={errors.state?.message}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ backgroundColor: '#eaeaea', padding: '17px', display: 'flex', justifyContent: 'space-between' }} >
                                <label style={{ color: '#727272' }}>Fecha de registro</label>
                                <input type="date" {...register('registerDate')} />

                            </div>
                        </Grid>

                        <Grid item xs={6} style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                            <Button type="submit" color="secondary" className="circular-btn" size='large' fullWidth>
                                Crear cuenta
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </form>
        </div>
    )
}

export default Register

