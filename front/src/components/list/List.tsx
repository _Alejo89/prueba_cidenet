import React, { useState, useEffect } from 'react';
import ClienteAxios from '../../axios/ClienteAxios';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { Box, Button, Grid, MenuItem, TextField, Typography } from '@mui/material'


interface Employee {
    firstName: string,
    secondName: string,
    lastNameOne: string,
    lastNameTwo: string,
    email: string,
    countryJob: string,
    identityType: string,
    identityNumber: string,
    admissionDate: string,
    area: string,
    state: string,
    registerDate: string,
}


const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'Primer Nombre', width: 130 },
    { field: 'secondName', headerName: 'Segundo Nombre', width: 130 },
    { field: 'lastNameOne', headerName: 'Primer Apellido', width: 90 },
    { field: 'lastNameTwo', headerName: 'Segundo Apellido', width: 160 },
    { field: 'email', headerName: 'Email', width: 160 },
    { field: 'countryJob', headerName: 'País de empleo', width: 160 },
    { field: 'identityType', headerName: 'Tipo de identidad', width: 160 },
    { field: 'identityNumber', headerName: 'Número de identidad', width: 160 },
    { field: 'admissionDate', headerName: 'Fecha de admisión', width: 160 },
    { field: 'area', headerName: 'Área', width: 160 },
    { field: 'state', headerName: 'Estado', width: 160 },
    { field: 'registerDate', headerName: 'Fecha de registro', width: 160 },
];



export default function DataTable() {

    const [allData, setAlldata] = useState<Employee[]>([]);

    const [rows, setRows] = useState<Employee[]>([]);
    console.log('rows', rows);

    useEffect(() => {
        getData();
    }, [])

    const getData = async () => {
        try {
            const response = await ClienteAxios.get('employees');
            setRows(response.data)
            setAlldata(response.data)
        } catch (error) {

        }
    }

    const search = (keyword: string) => {
        if (keyword === '') {
            setRows(allData);
            return;
        }
        const filterData = allData.filter(item => {

            if ((item.firstName === keyword) ||
                (item.secondName === keyword) ||
                (item.lastNameOne === keyword) ||
                (item.email === keyword) ||
                (item.countryJob === keyword) ||
                (item.identityNumber === keyword)) {
                console.log('item 2', item);

                return item;
            } else {
                console.log('item 3', item);

            }

        })

        setRows(filterData);
    }

    return (
        <div style={{ height: 400, width: '80%', marginRight: 'auto', marginLeft: 'auto', marginTop: '100px' }}>
            <Box sx={{ width: '30%', padding: '10px 20px', marginLeft: 'auto', marginRight: 'auto' }}>
                <Grid item xs={12}>
                    <TextField  label="Filtrar Empleado" type="text" variant="filled" fullWidth  onChange={(e) => search(e.target.value)} />
                </Grid>
            </Box>
            <DataGrid
                rows={rows}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
            />
        </div>
    );
}
