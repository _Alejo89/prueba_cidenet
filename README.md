Intrucciones para el levantamiento del sistema

-hay una carpeta "api" para el backend y otra carpeta "front" desarrollada con reactjs
-"npm i" para instalar las dependencias de cada carpeta 
-npm run dev para el back, npm start para el front

-en la raiz del directorio hay un archivo sql para la creacion de la bd
-en el backend se utilizo express y el orm Sequelize, el script sql es para el motor de bd Mysql

-para mirar la pantalla de registro http://localhost:3000/register (browser)
-para mirar la pantalla de la lista de empleados juento con el filtro de busqueda http://localhost:3000/list (browser)