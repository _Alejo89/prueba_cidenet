const passport = require('passport')
const localStrategy = require('passport-local').Strategy
// const User = require('../models').User
// const Role = require('../models').Role
const db = require('../models')


const JWTStrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt

passport.use('signup', new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
    try {
        console.log('email', email);
        console.log('password', password);
        const user = await db.User.create({ email, password })
        return done(null, user)
    } catch (e) {
        done(e)
    }
}))

passport.use('login', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
}, async (email, password, done) => {
    try {
        const user = await db.User.findOne({
            where: { email: email },
            include: [{
                model: db.Role
            }]
        })
        if (!user) {
            return done(null, false, { message: 'User not found' })
        }

        const validate = await user.isValidPassword(password)

        if (!validate) {
            return done(null, false, { message: 'Wrong password' })
        }

        return done(null, user, { message: 'Login successfull' })
    } catch (e) {
        return done(e)
    }
}))

passport.use(new JWTStrategy({
    secretOrKey: 'top_secret',
    // jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
    jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme("Bearer")

}, async (token, done) => {
    try {
        return done(null, token.user)
    } catch (e) {
        done(error)
    }
}))

