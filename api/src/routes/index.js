const express = require('express')
const router = express.Router()

const passport = require('passport')
const jwt = require('jsonwebtoken')

const EmployeeController = require('../controllers/EmployeeController');

router.post('/employee', /*passport.authenticate('jwt', { session: false })*/ EmployeeController.create)

/* GET home page. */
router.get('/employees', EmployeeController.showAll)

router.post('/signup', passport.authenticate('signup', { session: false }), async (req, res, next) => {
  res.json({
    message: 'Signup successful',
    user: req.user,
  })
})

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    try {
      if (err || !user) {
        console.log(err)
        const error = new Error('new Error')
        return next(error)
      }

      req.login(user, { session: false }, async (err) => {
        console.log('user', user);
        if (err) return next(err)
        const [role] = user.Roles;
        const body = { _id: user.id, name: user.name, email: user.email, role: role.name }

        const token = jwt.sign({ user: body }, 'top_secret', { expiresIn: '1d' })
        return res.json({ token, body })
      })
    }
    catch (e) {
      return next(e)
    }
  })(req, res, next)
})

router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  res.json({
    message: 'You did it!',
    user: req.user
  })
})

module.exports = router
