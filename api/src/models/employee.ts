'use strict';
import {
   Model
 } from 'sequelize';
interface EmployeeAttributes {
   firstName:string,
   secondName:string,
   lastNameOne:string,
   lastNameTwo:string,
   email:string,
   countryJob:string,
   identityType:string,
   identityNumber:string,
   admissionDate:string,
   area:string,
   state:string,
   registerDate:string,
 }
module.exports = (sequelize: any, DataTypes: any) => {
   class Employee extends Model<EmployeeAttributes> 
     implements EmployeeAttributes{
     /**
      * Helper method for defining associations.
      * This method is not a part of Sequelize lifecycle.
      * The `models/index` file will call this method automatically.
      */
      declare firstName:string;
      declare secondName:string;
      declare lastNameOne:string;
      declare lastNameTwo:string;
      declare email:string;
      declare countryJob:string;
      declare identityType:string;
      declare identityNumber:string;
      declare admissionDate:string;
      declare area:string;
      declare state:string;
      declare registerDate:string;
    static associate(models: any) {
       // define association here
 
     }
   };
   Employee.init({
 
     firstName: {
       type: DataTypes.STRING,
       allowNull: false
     },
     secondName: {
       type: DataTypes.STRING,
       allowNull: false
     },
     lastNameOne: {
       type: DataTypes.STRING,
       allowNull: false
     },
     lastNameTwo: {
       type: DataTypes.STRING,
       allowNull: false
     },
     email: {
       type: DataTypes.STRING,
       allowNull: false
     },
     countryJob: {
       type: DataTypes.STRING,
       allowNull: false
     },
     identityType: {
       type: DataTypes.STRING,
       allowNull: false
     },
     identityNumber: {
       type: DataTypes.STRING,
       allowNull: false
     },
     admissionDate: {
       type: DataTypes.STRING,
       allowNull: false
     },
     area: {
       type: DataTypes.STRING,
       allowNull: false
     },
     state: {
       type: DataTypes.STRING,
       allowNull: false
     },
     registerDate: {
       type: DataTypes.STRING,
       allowNull: false
     }
   }, {
     sequelize,
     modelName: 'Employee',
   });
   return Employee;
 };

