// 'use strict';
// module.exports = (sequelize, DataTypes) => {
//   const Role = sequelize.define('Role', {
//     name: DataTypes.STRING
//   }, {});
//   Role.associate = function(models) {
//     Role.belongsToMany(models.User, { through: 'users_roles', foreignKey: "role_id", });
//   };
//   return Role;
// };



'use strict';
import {
  Model
} from 'sequelize';
interface RoleAttributes {
  name: string
}
module.exports = (sequelize: any, DataTypes: any) => {
  class Role extends Model<RoleAttributes>
    implements RoleAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    declare name: string;

    static associate(models: any) {
      // define association here
      Role.belongsToMany(models.User, { through: 'users_roles', foreignKey: "role_id", });
    }
  };
  Role.init({

    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Role',
  });
  return Role;
};

