'use strict';
const bcrypt = require('bcrypt')
// const {
//   Model
// } = require('sequelize');
// module.exports = (sequelize, DataTypes) => {

//   const User = sequelize.define('User', {
//     name: DataTypes.STRING,
//     email: DataTypes.STRING,
//     password: DataTypes.STRING
//   }, {});
//   User.associate = function(models) {
//     // associations can be defined here
//     User.belongsToMany(models.Role, { through: 'users_roles', foreignKey: "user_id", });
//   };

//   User.addHook('beforeCreate', async(user, options) => {
//     console.log('user.password', user.password);
//     const hash = await bcrypt.hash(user.password, 10)
//     user.password = hash
//     //next()
//   });

//   User.prototype.isValidPassword = async function (password) {
//     const user = this;
//     const compare = await bcrypt.compare(password, user.password)
//     return compare
//   }

//   return User;
// };




// 'use strict';
import {
  Model
} from 'sequelize';
interface UserAttributes {
  name: string,
  email: string,
  password: string
}
module.exports = (sequelize: any, DataTypes: any) => {
  class User extends Model<UserAttributes>
    implements UserAttributes {
    // constructor() {
    //   super();
    //   // ...code
    //   this.isValidPassword = isValidPassword; //becomes own property
    // }
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    declare isValidPassword: any;
    declare name: string;
    declare email: string;
    declare password: string;


    static associate(models: any) {
      // define association here
      User.belongsToMany(models.Role, { through: 'users_roles', foreignKey: "user_id", });

    }
  };
  User.init({

    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'User',
  });

  User.addHook('beforeCreate', async (user: any, options) => {
    console.log('user.password', user.password);
    const hash = await bcrypt.hash(user.password, 10)
    user.password = hash
    //next()
  });

  User.prototype.isValidPassword = async function (password: any) {
    
    const user: any = this;
    const compare = await bcrypt.compare(password, user.dataValues.password)
    return compare
  }
  //User.prototype.isValidPassword = isValidPassword;
  return User;
};

