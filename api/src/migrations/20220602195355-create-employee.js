'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      secondName: {
        type: Sequelize.STRING
      },
      lastNameOne: {
        type: Sequelize.STRING
      },
      lastNameTwo: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      countryJob: {
        type: Sequelize.STRING
      },
      identityType: {
        type: Sequelize.STRING
      },
      identityNumber: {
        type: Sequelize.STRING
      },
      admissionDate: {
        type: Sequelize.DATE
      },
      area: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.STRING
      },
      registerDate: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Employees');
  }
};