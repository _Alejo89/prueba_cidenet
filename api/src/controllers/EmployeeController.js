const db = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
console.log('db', db);

module.exports = {
    // // CREATE
     create(req, res) {
        console.log('req', req.body.data);
        db.Employee.create(
            req.body.data
        ).then(employees => {
            res.json(employees);
        }).catch((error=>{
            console.log('ERROR', error);
            res.json(error);
        }))
    },

      // INDEX /api/productos
      showAll(req, res) {
        console.log('entro');
        db.Employee.findAll().then(employees => {
            res.json(employees);
        }).catch((error=>{
            console.log('ERROR', error);
            res.json(error);
        }))
    },
}