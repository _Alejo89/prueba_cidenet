const express = require('express')
const bodyParser = require('body-parser')
var cors = require('cors')
const routes = require('./routes')
const { sequelize } = require('./models/index');

const app = express()

require('./auth/auth')

app.use(cors())
app.use(bodyParser.json())
app.use(routes)

const PORT = 3005

app.listen(PORT, function () {
  console.log(`App listening on ${PORT}`)
  sequelize.sync({ force: false }).then(() => {
    console.log("Se ha establecido la conexión");
  })
})